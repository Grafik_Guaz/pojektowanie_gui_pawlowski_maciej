/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication2;
import java.util.Random;
import java.io.Serializable;

/**
 *
 * @author guaz
 */
public class CoreAppData implements Serializable {
    
    /**
     * @param args the command line arguments
     */
    private static Random rand = new Random();
    private static int min_range = 1, max_range = 100, rolled, attemps;
    private static int timeSecound, timeMinute, timeHour;

    public CoreAppData() {
        
    }
    /*
    public CoreAppData(int min_range, int max_range, int rolled, int attemps, int timeSecound, int timeMinute, int timeHour) {
        this.min_range = min_range;
        this.max_range = max_range;
        this.rolled = rolled;
        this.attemps = attemps;
        this.timeSecound = timeSecound;
        this.timeMinute = timeMinute;
        this.timeHour = timeHour;
    }
    */
    
    public static void main(String[] args) {
        
    }

    public void generate(){
        rolled = rand.nextInt((max_range-min_range+1))+min_range;
        attemps = 0;
        timeSecound = 0;
        timeMinute = 0;
        timeHour = 0;
    }

    public static int check(int shot){
        attemps++;
        if (shot < rolled) {
            return -1;
        } else if (shot > rolled) {
            return 1;
        } else {
            return 0;
        }
    }

    public void setMinMax(int minR, int maxR){
        min_range = minR;
        max_range = maxR;
    }
    
    public String getMinMax(){
        return "<"+Integer.toString(min_range)+", "+Integer.toString(max_range)+">";
    }
    
    public int getMin(){
        return min_range;
    }
    
    public int getMax(){
        return max_range;
    }

    public int getAttemps(){
        return attemps;
    }
    
    public void resetAttemps(){
        attemps = 0;
    }
    
    public void resetTimer(){
        timeSecound = 0;
        timeMinute = 0;
        timeHour = 0;
    }
    
    public void increaseTimer(){
        timeSecound += 1;
        if (timeSecound == 60) {
            timeSecound = 0;
            timeMinute += 1;
            if (timeMinute == 60) {
                timeMinute = 0;
                timeHour += 1;
            }
        }
    }
    
    public int getTimeSecound(){
        return timeSecound; 
    }
    
    public int getTimeMinute(){
        return timeMinute; 
    }
    
    public int getTimeHour(){
        return timeHour; 
    }
}
