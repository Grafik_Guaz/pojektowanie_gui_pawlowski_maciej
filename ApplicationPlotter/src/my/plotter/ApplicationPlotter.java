/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package my.plotter;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Line2D;
import java.awt.Polygon;
import java.util.*;

/**
 *
 * @author guaz
 */
public class ApplicationPlotter extends javax.swing.JFrame {

    /**
     * Creates new form ApplicationPlotter
     */
    public ApplicationPlotter() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jTextField2 = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jTextField3 = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jTextField4 = new javax.swing.JTextField();
        jButton7 = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jButton8 = new javax.swing.JButton();
        jButton9 = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("FUNCTION PLOTTER");
        setResizable(false);

        jButton1.setText("Linear f.");
        jButton1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton1MouseClicked(evt);
            }
        });

        jButton2.setText("Quadratic f.");
        jButton2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton2MouseClicked(evt);
            }
        });

        jButton3.setText("Cubic f.");
        jButton3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton3MouseClicked(evt);
            }
        });

        jButton4.setText("Absolute v. f.");
        jButton4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton4MouseClicked(evt);
            }
        });

        jButton5.setText("Signum f.");
        jButton5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton5MouseClicked(evt);
            }
        });

        jLabel1.setText("data ( a )");

        jTextField1.setText("1");

        jLabel2.setText("data ( b )");

        jTextField2.setText("0");

        jLabel3.setText("data ( c )");

        jTextField3.setText("0");

        jLabel4.setText("data ( d )");

        jTextField4.setText("0");

        jButton7.setText("Clear All");
        jButton7.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton7MouseClicked(evt);
            }
        });

        jPanel2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 398, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 398, Short.MAX_VALUE)
        );

        jButton8.setText("Sine f.");
        jButton8.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton8MouseClicked(evt);
            }
        });

        jButton9.setText("Cosine f.");
        jButton9.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton9MouseClicked(evt);
            }
        });

        jLabel5.setText("Draw function:");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2)
                    .addComponent(jLabel4)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addGap(14, 14, 14)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 26, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel5)
                .addGap(15, 15, 15))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jButton1)
                                .addGap(18, 18, 18)
                                .addComponent(jButton2)
                                .addGap(18, 18, 18)
                                .addComponent(jButton3))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addComponent(jButton4)
                                .addGap(18, 18, 18)
                                .addComponent(jButton5)
                                .addGap(18, 18, 18)
                                .addComponent(jButton8))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(5, 5, 5)
                                .addComponent(jLabel3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel4)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(18, 18, 18)
                        .addComponent(jButton9)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton7))
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(14, 14, 14))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jButton2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton2MouseClicked
        // Quadratic function button
        drawXYAxis(); // Draws coordinate plane
        
        Graphics g = (Graphics) jPanel2.getGraphics();
        g.translate(200, 200); // New plain orgin

        Random rand = new Random();
        int x1 = rand.nextInt(255);
        int x2 = rand.nextInt(255);
        int x3 = rand.nextInt(255);
        g.setColor(new Color(x1, x2, x3)); // Random color
        
        float a = Float.parseFloat(jTextField1.getText());
        float b = Float.parseFloat(jTextField2.getText());
        float c = Float.parseFloat(jTextField3.getText());
        float s = 100.0f; // scale factor
        
        Polygon pQF = new Polygon();
        for (int x = -200; x <= 200; ++x) {
            pQF.addPoint(x, -(int) (s * (a * Math.pow(x / s, 2) + b * (x / s) + c)));
        }
        g.drawPolyline(pQF.xpoints, pQF.ypoints, pQF.npoints);
    }//GEN-LAST:event_jButton2MouseClicked

    private void jButton7MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton7MouseClicked
        // Clear button
        repaint();
        jTextField1.setText("1");
        jTextField2.setText("0");
        jTextField3.setText("0");
        jTextField4.setText("0");
    }//GEN-LAST:event_jButton7MouseClicked

    private void jButton1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton1MouseClicked
        // Linear function button
        // f(x) = a * x + b
        drawXYAxis(); // Draws coordinate plane
        
        Graphics2D g = (Graphics2D) jPanel2.getGraphics();
        g.translate(200, 200); // New plain orgin
        
        Random rand = new Random();
        int x1 = rand.nextInt(255);
        int x2 = rand.nextInt(255);
        int x3 = rand.nextInt(255);
        g.setColor(new Color(x1, x2, x3)); // Random color
        
        float a = Float.parseFloat(jTextField1.getText());
        float b = Float.parseFloat(jTextField2.getText());
        float s = 100.0f; // scale factor
        
        float[] x = {-200.0f, 200.0f};
        float[] y = new float[2];
        y[0] = s * (a * (x[0] / s) + b);
        y[1] = s * (a * (x[1] / s) + b);
        g.draw(new Line2D.Float(x[0], -y[0], x[1], -y[1]));
    }//GEN-LAST:event_jButton1MouseClicked

    private void jButton8MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton8MouseClicked
        // Sine function button
        drawXYAxis(); // Draws coordinate plane
        
        Graphics g = (Graphics) jPanel2.getGraphics();
        g.translate(200, 200); // New plain orgin
        
        Random rand = new Random();
        int x1 = rand.nextInt(255);
        int x2 = rand.nextInt(255);
        int x3 = rand.nextInt(255);
        g.setColor(new Color(x1, x2, x3)); // Random color

        float a = Float.parseFloat(jTextField1.getText());
        float s = 100.0f; // scale factor
        
        Polygon pSine = new Polygon();
        for (int x = -200; x <= 200; ++x) {
            pSine.addPoint(x, -(int) (s * Math.sin((x / s) * a)));
        }
        g.drawPolyline(pSine.xpoints, pSine.ypoints, pSine.npoints);
    }//GEN-LAST:event_jButton8MouseClicked

    private void jButton9MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton9MouseClicked
        // Cosine function button
        drawXYAxis(); // Draws coordinate plane
        
        Graphics g = (Graphics) jPanel2.getGraphics();
        g.translate(200, 200); // New plain orgin
        
        Random rand = new Random();
        int x1 = rand.nextInt(255);
        int x2 = rand.nextInt(255);
        int x3 = rand.nextInt(255);
        g.setColor(new Color(x1, x2, x3)); // Random color
        
        float a = Float.parseFloat(jTextField1.getText());
        float s = 100.0f; // scale factor
        
        Polygon pCosine = new Polygon();
        for (int x = -200; x <= 200; ++x) {
            pCosine.addPoint(x, -(int) (s * Math.cos((x / s) * a)));
        }
        g.drawPolyline(pCosine.xpoints, pCosine.ypoints, pCosine.npoints);
    }//GEN-LAST:event_jButton9MouseClicked

    private void jButton3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton3MouseClicked
        // Cubic function button
        drawXYAxis(); // Draws coordinate plane
        
        Graphics g = (Graphics) jPanel2.getGraphics();
        g.translate(200, 200); // New plain orgin
        
        Random rand = new Random();
        int x1 = rand.nextInt(255);
        int x2 = rand.nextInt(255);
        int x3 = rand.nextInt(255);
        g.setColor(new Color(x1, x2, x3)); // Random color
        
        float a = Float.parseFloat(jTextField1.getText());
        float b = Float.parseFloat(jTextField2.getText());
        float c = Float.parseFloat(jTextField3.getText());
        float d = Float.parseFloat(jTextField4.getText());
        float s = 100.0f; // scale factor
        
        Polygon pCF = new Polygon();
        for (int x = -200; x <= 200; ++x) {
            pCF.addPoint(x, -(int) (s * (a * Math.pow(x / s, 3) + b * Math.pow(x / s, 2) + c * (x / s) + d)));
        }
        g.drawPolyline(pCF.xpoints, pCF.ypoints, pCF.npoints);
    }//GEN-LAST:event_jButton3MouseClicked

    private void jButton4MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton4MouseClicked
        // Absolute value function button
        // f(x) = a * |x - b| + c
        drawXYAxis(); // Draws coordinate plane
        
        Graphics g = (Graphics) jPanel2.getGraphics();
        g.translate(200, 200); // New plain orgin
        
        Random rand = new Random();
        int x1 = rand.nextInt(255);
        int x2 = rand.nextInt(255);
        int x3 = rand.nextInt(255);
        g.setColor(new Color(x1, x2, x3)); // Random color

        float a = Float.parseFloat(jTextField1.getText());
        float b = Float.parseFloat(jTextField2.getText());
        float c = Float.parseFloat(jTextField3.getText());
        float s = 100.0f; // scale factor
        
        Polygon pAVF = new Polygon();
        for (int x = -200; x <= 200; ++x) {
            pAVF.addPoint(x, -(int) (s * ((a * Math.abs(x / s - b)) + c)));
        }
        g.drawPolyline(pAVF.xpoints, pAVF.ypoints, pAVF.npoints);
    }//GEN-LAST:event_jButton4MouseClicked

    private void jButton5MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton5MouseClicked
        // Signum function button
        // f(x) = sgn(x)
        drawXYAxis(); // Draws coordinate plane
        
        Graphics g = (Graphics) jPanel2.getGraphics();
        g.translate(200, 200); // New plain orgin
        
        Random rand = new Random();
        int x1 = rand.nextInt(255);
        int x2 = rand.nextInt(255);
        int x3 = rand.nextInt(255);
        g.setColor(new Color(x1, x2, x3)); // Random color
        
        Polygon pSFbz = new Polygon();
        for (int x = -200; x < 0; ++x) {
            pSFbz.addPoint(x, -100 * (int) Math.signum(x));
        }
        g.drawPolyline(pSFbz.xpoints, pSFbz.ypoints, pSFbz.npoints);

        Polygon pSFz = new Polygon();
        pSFz.addPoint(0, 0);
        g.drawPolyline(pSFz.xpoints, pSFz.ypoints, pSFz.npoints);

        Polygon pSFaz = new Polygon();
        for (int x = 1; x <= 200; ++x) {
            pSFaz.addPoint(x, -100 * (int) Math.signum(x));
        }
        g.drawPolyline(pSFaz.xpoints, pSFaz.ypoints, pSFaz.npoints);
    }//GEN-LAST:event_jButton5MouseClicked

    private void drawXYAxis()
    {
        Graphics g = (Graphics) jPanel2.getGraphics();
        g.translate(200, 200); // New plane orgin
        g.setColor(new Color(0, 0, 0)); // Black color
        g.drawLine(-200, 0, 200, 0); // X axis
        g.drawString("X", 180, 20); // Under X axis
        g.drawLine(0, 200, 0, -200); // Y axis
        g.drawString("Y", 10, -180); // Next to Y axis
        g.drawLine(100, 5, 100, -5); // On X axis right
        g.drawString("1", 98, 20); // Under X axis right
        g.drawLine(-100, 5, -100, -5); // On X axis left
        g.drawString("-1", -105, 20); // Under X axis left
        g.drawLine(5, -100, -5, -100); // On Y axis top
        g.drawString("1", 10, -95); // Next to Y axis top
        g.drawLine(5, 100, -5, 100); // On Y axis bottom
        g.drawString("-1", 10, 105); // Next to Y axis bottom
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ApplicationPlotter.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ApplicationPlotter.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ApplicationPlotter.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ApplicationPlotter.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ApplicationPlotter().setVisible(true);
                
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton7;
    private javax.swing.JButton jButton8;
    private javax.swing.JButton jButton9;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField2;
    private javax.swing.JTextField jTextField3;
    private javax.swing.JTextField jTextField4;
    // End of variables declaration//GEN-END:variables
}
