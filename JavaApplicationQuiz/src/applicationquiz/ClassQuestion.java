/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package applicationquiz;
import java.io.Serializable;

/**
 *
 * @author guaz
 */
public class ClassQuestion implements Serializable {
    public String loadedImageName;
    // default empty
    public String questionContent;
    // must to set
    public String[] answersContent;
    // must to set if questionType is other then 3
    public int[] pointedAnswers;
    // -1 - default for wrong answer when negative points is selected
    // 0 - default for wrong answer
    // 1 - default for correct answer
    public short questionType;
    // 0 - default one question is correct
    // 1 - many or none question is correct
    // 2 - true or false question 
    // 3 - open question, with textfield for user
    public boolean defaultPoints;
    public boolean negativePoints;
    public boolean created = false;

    public void createQuestion(String image, String question, String[] answers, int[] points, short questiontype, boolean defaultpoints, boolean negativepoints) {
        loadedImageName = image;
        questionContent = question;
        answersContent = answers;
        pointedAnswers = points;
        questionType = questiontype;
        defaultPoints = defaultpoints;
        negativePoints = negativepoints;
        created = true;
    }
    public String getImage(){ return loadedImageName; }
    public String getQuestion(){ return questionContent; }
    public String[] getAnswers(){ return answersContent; }
    public int[] getPoints(){ return pointedAnswers; }
    public short getQType(){ return questionType; }
    public boolean isCreated() { return created; }
    public boolean isPointsDefault() { return defaultPoints; }
    public boolean isNegativelyPoint() { return negativePoints; }
    
    
}
