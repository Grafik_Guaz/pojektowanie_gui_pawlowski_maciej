/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package applicationquiz;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/**
 *
 * @author guaz
 */
public class ClassQuiz implements Serializable {
    public static int NUM_OF_QUESTION_CAP_VALUE = 60;
    public short numberOfQuestions = 1;
    public short indexQuestion = 0;
    public ClassQuestion[] questions = new ClassQuestion[NUM_OF_QUESTION_CAP_VALUE];
    
    public ClassQuiz(){
        numberOfQuestions = 1;
        indexQuestion = 0;
    }
    
    public ClassQuestion get_current_question(){
        if (questions[indexQuestion] != null){
            return questions[indexQuestion];
        } else {
            questions[indexQuestion] = new ClassQuestion();
            return questions[indexQuestion];
        }
    }
    
    public boolean nextQuestion() {
        indexQuestion++;
        if (numberOfQuestions == NUM_OF_QUESTION_CAP_VALUE){
            return false; //Add next one question isn't available
        }
        if (indexQuestion >= numberOfQuestions){
            questions[indexQuestion] = new ClassQuestion();
            numberOfQuestions++;
            return true; //Add new question
        } else {
            return true; //Get previously defined question
        }
    }
    
    public boolean previousQuestion() {
        indexQuestion--;
        if (indexQuestion < 0) {
            indexQuestion++;
            return false; //Previous question doesn't exist
        }
        return true; //Get previously defined question
    }
        
    public void backToFirstQuestion(){
        indexQuestion = 0;
    }
    
    public int getCurrentQuestionNumber(){
        return indexQuestion+1;
    }
    
    public int getCurrentQuestionCreated(){
        return numberOfQuestions;
    }
    
    
}
